package com.itsigned.huqariq.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import kotlinx.android.synthetic.main.android10.*


class IntroFourFragment : StepFragment() {

    var action: IntroActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android10, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        layoutIntroFourFragment.setOnClickListener {
            val intent = Intent(activity, VariationActivityTour::class.java)
            startActivity(intent)
        }
    }

}