package com.itsigned.huqariq.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import kotlinx.android.synthetic.main.android14.*

class VariationThreeFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android14, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        btnNextThree.setOnClickListener {
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutVariationTour, VariationResultFragment())
            at.addToBackStack(null)
            at.commit()
        }
    }

}