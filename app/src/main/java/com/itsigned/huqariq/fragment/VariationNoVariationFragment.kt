package com.itsigned.huqariq.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import kotlinx.android.synthetic.main.android13.*

class VariationNoVariationFragment: Fragment() {

    var portal:VariationActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android13, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        portal?.changeBackground(true)
        configureActionButton()
    }

    private fun configureActionButton() {
        btnNextNoVar.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
            portal?.changeBackground(false)
        }
    }

}