package com.itsigned.huqariq.activity

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.itsigned.huqariq.R
import com.itsigned.huqariq.fragment.IntroOneFragment
import com.itsigned.huqariq.fragment.VariationOneFragment

class VariationActivityTour : AppCompatActivity(), VariationActivityTourInterface {

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_variation_tour)
        backGroundColor()
        supportFragmentManager.beginTransaction().add(R.id.layoutVariationTour, VariationOneFragment()).commit()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
    }

    override fun changeBackground(estado: Boolean) {
        if (estado) window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
        else window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
    }

}

interface VariationActivityTourInterface {
    fun changeBackground(estado: Boolean)
}