package com.itsigned.huqariq.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.itsigned.huqariq.R
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.util.session.SessionManager
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import java.util.*


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)
        backGroundColor()

        AppCenter.start(application, "9080c742-2242-4b86-8e4f-c971edeb4158",
                Analytics::class.java, Crashes::class.java)

        val task: TimerTask = object : TimerTask() {
            override fun run() {
                val isLogued=SessionManager.getInstance(this@SplashActivity).isLogged
                if (isLogued) goToActivity() else goToActivity(StartActivity::class.java)
            }
        }
        val timer = Timer()
        timer.schedule(task, 4000)
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_android_1)
    }
}