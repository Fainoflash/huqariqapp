package com.itsigned.huqariq.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import kotlinx.android.synthetic.main.android8.*


class IntroTwoFragment : StepFragment() {

    var action: IntroActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android8, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        layoutIntroTwoFragment.setOnClickListener {
            action?.goNextStep()
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutIntroTour, IntroThreeFragment())
            at.addToBackStack(null)
            at.commit()
        }
    }

}