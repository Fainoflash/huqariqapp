package com.itsigned.huqariq.activity

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.fragment.*
import kotlinx.android.synthetic.main.android7_10stepper.*




class IntroActivityTour : AppCompatActivity(), IntroActivityTourInterface {

    private var countTabs=4
    private var position=0

    private var currentFragment: Fragment? = null

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro_tour)
        backGroundColor()
        goNextStep()
        supportFragmentManager.beginTransaction().add(R.id.layoutIntroTour, IntroOneFragment()).commit()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.color.white)
    }

    private fun transaction(fragment: Fragment) {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        currentFragment = fragment
        ft.add(R.id.layoutIntroTour, currentFragment!!)
        ft.addToBackStack(null)
        ft.commit()
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layoutLoginTour, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun updateStepper(){
        if(position>countTabs)return

        if(countTabs==3){
            introFour.visibility=View.GONE
        }
        val sdk = android.os.Build.VERSION.SDK_INT
        val view=getViewStep()
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.circle) )
        } else {
            view.background = ContextCompat.getDrawable(this, R.drawable.circle)
        }
        /*if(position==3){
            buttonNext.text = getString(R.string.view_register_complete_buttom)
        }

        if(position==2 && countTabs==3){
            buttonNext.text = getString(R.string.view_register_complete_buttom)
        }*/

    }

    private fun getViewStep():View{
        return when(position){
            1 ->introOne
            2 ->introTwo
            3 ->introThree
            4 ->introFour
            else ->throw Exception("out limit step")

        }
    }

    override fun goNextStep() {
        position++
        updateStepper()
        /*transaction(fragment)*/
    }

}

interface IntroActivityTourInterface{
    fun goNextStep()

}