package com.itsigned.huqariq.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import kotlinx.android.synthetic.main.activity_login.*


class LogInFragment : StepFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        /*btnCreaCuenta.setOnClickListener {
            val intent = Intent(this, RegisterActivityn::class.java)
            startActivity(intent)
        }*/
        tv_forget_password.setOnClickListener {
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutLoginTour, NewPassAccFragment())
            at.addToBackStack(null)
            at.commit()
        }
    }

}