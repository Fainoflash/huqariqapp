package com.itsigned.huqariq.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTour
/*import com.itsigned.huqariq.activity.RegisterActivityInterface*/
import com.itsigned.huqariq.activity.RegisterActivityTour
import kotlinx.android.synthetic.main.android3.*


class RegisterFragment : StepFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        btnCreaCuenta.setOnClickListener {
            val intent = Intent(activity, IntroActivityTour::class.java)
            startActivity(intent)
        }
        textoCondicionesCuenta.setOnClickListener {
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutRegisterTour, UsageConditionsFragment())
            at.addToBackStack(null)
            at.commit()
        }
    }

}