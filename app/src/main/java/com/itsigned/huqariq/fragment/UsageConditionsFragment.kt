package com.itsigned.huqariq.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import kotlinx.android.synthetic.main.android6.*

class UsageConditionsFragment : StepFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android6, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        regresarCondiciones.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
    }
}