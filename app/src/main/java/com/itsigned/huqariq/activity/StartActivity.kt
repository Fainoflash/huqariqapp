package com.itsigned.huqariq.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.itsigned.huqariq.R
import com.itsigned.huqariq.helper.PermissionHelper
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.helper.hasErrorEditTextEmpty
import com.itsigned.huqariq.helper.showError
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.LoginRequestDto
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.android1n.*

private const val REQUEST_SIGNUP = 0
class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.android1n)
        if (SessionManager.getInstance(this).isLogged) goToActivity()
        configureActionButton()
        backGroundColor()
        PermissionHelper.recordAudioPermmision(this, null)

    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_android_1)
    }

    /**
     * Metodo con las configuraciones iniciales de los botones
     */
    private fun configureActionButton() {
        btnRegistrese.setOnClickListener {
            val intent = Intent(this, RegisterActivityTour::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
        Text_Ingrese_Aqui.setOnClickListener {
            val intent = Intent(this, LoginActivityTour::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
    }
}