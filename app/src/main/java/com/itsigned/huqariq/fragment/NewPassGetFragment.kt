package com.itsigned.huqariq.fragment

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import com.itsigned.huqariq.helper.ValidationHelper
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.helper.setErrorEditText
import com.itsigned.huqariq.helper.showMessage
import com.itsigned.huqariq.model.FormRegisterUserStepOneDto
import com.itsigned.huqariq.model.RequestValidateDni
import com.itsigned.huqariq.model.RequestValidateMail
import com.itsigned.huqariq.model.ResponseValidateDni
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.android27.*
import java.util.*


class NewPassGetFragment : StepFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android27, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        /*btnCreaCuenta.setOnClickListener {
            val intent = Intent(this, RegisterActivityn::class.java)
            startActivity(intent)
        }*/
        btnNewPassGenerate.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack(0,FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }
}