package com.itsigned.huqariq.activity

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.fragment.*
import kotlinx.android.synthetic.main.android21.*
import kotlinx.android.synthetic.main.android_nav_bar.*

class MainActivityTour: AppCompatActivity() {

    private  var currentFragment: Fragment?=null

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_tour)
        /*backGroundColor()*/
        configureActionButton()
        supportFragmentManager.beginTransaction().add(R.id.layoutMainTour, MainStartFragment()).commit()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.color.white)
    }

    private fun configureActionButton() {
        navBarStart.setOnClickListener {
            changeNavBar(0)
        }

        navBarConfig.setOnClickListener {
            changeNavBar(1)
        }

        navBarAbout.setOnClickListener {
            changeNavBar(2)
        }
    }

    private fun changeNavBar(navIndex: Int) {
        when (navIndex) {
            0 -> currentFragment=MainStartFragment()
            1 -> currentFragment=MainConfigFragment()
            2 -> currentFragment=MainAboutFragment()
        }
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.layoutMainTour, currentFragment!!)
        ft.commit()

    }

}