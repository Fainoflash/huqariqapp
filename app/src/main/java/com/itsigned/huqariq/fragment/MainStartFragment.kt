package com.itsigned.huqariq.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import kotlinx.android.synthetic.main.android21.*

class MainStartFragment : Fragment() {

    var action: IntroActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android21, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    private fun configureActionButton() {
        constraintGrabarFrasesLinear.setOnClickListener {
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, MainPhrasesFragment())
            at.addToBackStack(null)
            at.commit()
        }

        constraintVerProgresoLinear.setOnClickListener {
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, MainProgressFragment())
            at.addToBackStack(null)
            at.commit()
        }
    }

}