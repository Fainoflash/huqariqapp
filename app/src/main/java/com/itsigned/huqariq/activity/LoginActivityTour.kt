package com.itsigned.huqariq.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.bean.User
import com.itsigned.huqariq.fragment.*
import com.itsigned.huqariq.helper.getLoadingProgress
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.FormRegisterStepThreeDto
import com.itsigned.huqariq.model.FormRegisterUserStepOneDto
import com.itsigned.huqariq.model.FormRegisterUserStepTwoDto
import com.itsigned.huqariq.model.LoginRequestDto
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.android27.*
import kotlinx.android.synthetic.main.stepper_header.*
import java.util.*


class LoginActivityTour : AppCompatActivity() {

    private var currentFragment: Fragment? = null

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.android3)
        backGroundColor()
        configureActionButton()
        /*customProgressDialog=getLoadingProgress()
        if(intent.extras!=null&&intent.extras!!.containsKey("email")){
            mapValues["email"]=intent.getStringExtra("email")
        }
        transaction()
        buttonNext.setOnClickListener { (currentFragment as StepFragment).verifyStep() }*/

    }*/

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_tour)
        transaction()
        backGroundColor()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
    }

    /**
     * Metodo con las configuraciones iniciales de los botones
     */
    private fun configureActionButton() {
        /*btnCreaCuenta.setOnClickListener {
            val intent = Intent(this, RegisterActivityn::class.java)
            startActivity(intent)
        }*/
        tv_forget_password.setOnClickListener {
            loadFragment(UsageConditionsFragment())
        }
    }

    private fun transaction() {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        currentFragment = LogInFragment()
        ft.add(R.id.layoutLoginTour, currentFragment!!)
        ft.commit()
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layoutLoginTour, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}